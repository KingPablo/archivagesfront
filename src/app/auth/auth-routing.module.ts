import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {LoginComponent} from './login/login.component';
import { MotDePasseOublierComponent } from './mot-de-passe-oublier/mot-de-passe-oublier.component';
import { RegisterComponent }from './register/register.component';
const routes: Routes = [

      {
        path: '', 
        redirectTo: 'login', 
        pathMatch: 'full'
      },
      
      {
        path: 'login',
        component: LoginComponent,
      },
      {
        path: 'pageMotDePasseOublier',
        component: MotDePasseOublierComponent,
      },
      {
        path: 'register',
        component: RegisterComponent,
      }

    
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
